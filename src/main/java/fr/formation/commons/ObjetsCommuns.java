package fr.formation.commons;

public class ObjetsCommuns {
	
	public class Url{
		 public static final String VUE_CREATION_PERSONNE = "/WEB-INF/creationPersonne.jsp";
		 public static final String VUE_CONNEXION = "/WEB-INF/connexion.jsp";
		 public static final String VUE_PUBLIC = "/public.jsp";
		 public static final String VUE_ESPACE_PRIVE = "/private/private.jsp";
		 public static final String VUE_ESPACE_PRIVE_ADMIN = "/privateAdmin/private.jsp";
		 public static final String VUE_ESPACE_FORMATEUR = "/formateurServlet";
		 public static final String VUE_ESPACE_MEMBRE = "/private/espaceMembre";
		 public static final String VUE_CREATION_FORMATION = "/privateAdmin/creationFormation.jsp";
		 public static final String VUE_ESPACE_SUPERADMIN = "/privateSuperAdmin/allPersonne.jsp";
		 public static final String VUE_ALL_PERSONNE = "/WEB-INF/allPersonne.jsp";
		 
	}
	
	public class Param{
			public static final String CHAMP_EMAIL = "email";
		    public static final String CHAMP_PASS = "motdepasse";
		    public static final String CHAMP_CONF = "confirmation";
		    public static final String CHAMP_NOM = "nom";
		    public static final String CHAMP_PRENOM = "prenom";
		    public static final String CHAMP_MEMOIRE = "memoire";
		    public static final String CHAMP_DATE_CONNECTION = "dateDernierConnexion";
		    public static final String CHAMP_MESSAGE = "message";
		    public static final String CHAMP_FORMATIONS_FORMATEUR = "mesFormations";
		    public static final String CHAMP_FORMATION_LABEL = "label";
		    public static final String CHAMP_FORMATIONS_DESCRIPTION= "description";
	}
	
	public class Beans{
		public static final String PERSONNE_VO = "personneVo";
		public static final String MAP_ERREURS = "erreurs"; 
		public static final String ALL_FORMATION = "formations";
		public static final String MES_ID_FORMATION = "mesIdFormations";
		public static final String ALL_PERSONNE = "personnes";
		
		
	}
	
	public class SessionBeans{
		public static final String SESSION_PERSONNE= "sessionPersonne";
		public static final String SESSION_FORMATIONS= "sessionFormation";
	}
	
	public class Cookies{
		public static final String COOKIES_CONNECT = "cookieConnect";
		
	}

}
