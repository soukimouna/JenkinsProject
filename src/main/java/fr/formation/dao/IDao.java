package fr.formation.dao;

import java.sql.SQLException;
import java.util.List;

import fr.formation.buisness.beans.Formation;
import fr.formation.buisness.beans.PersonneDao;



public interface IDao {
	
	public PersonneDao findPersonneByUtilisateur(PersonneDao p);
	public PersonneDao createPersonne(PersonneDao p) throws SQLException;
	public List<Formation> getListeFormationParPersonne(int id);
	public void creationFormation(Formation f);
	public  List<Formation> recupererAllFormation();
	public void inscriptionFormation(int idPersonne,int idFormation);
	List<Formation> getListeFormationParFormateur(int id);
	public List<PersonneDao> getAllPersonnes() throws SQLException;

	
}
