package fr.formation.services;

import java.util.List;

import fr.formation.buisness.beans.Formation;

public interface IServiceFormation {

	public List<Formation> getListFormationParIdPersonne(int id);
	public void creerFormation(Formation f);
	public List<Formation> recupererListeFormation();
	public void inscriptionFormation(int idPersonne, int idFormation);
	public List<Formation> getListFormationParIdFormateur(int id);
	
}
