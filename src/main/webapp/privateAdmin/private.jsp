<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Acces restreint avec filtre ADMIN</title>
    </head>
    <body>
    <c:import url="/privateAdmin/menu.jsp" /><hr>
     <h3> Espace prive Formateur</h3>
      
      <b>Liste de mes formations</b><br>
      <table  border="1">
      	<tr>
      		<td> Id Formation</td>
      		<td> libelle formation </td>
      		<td> Description</td>
      		<td> Nb Participant </td>
      	</tr>
      <c:forEach var="item" items="${mesFormations}">
      	<tr>
			<td><c:out value="${item.id}" /> </td>
			<td><c:out value="${item.label}" />  </td>
			<td><c:out value="${item.description}" /> </td>
			<td><c:out value="${item.nombreParticipant}" /> </td>
		</tr>
		</c:forEach>
     </table>
   			
    </body>
</html>